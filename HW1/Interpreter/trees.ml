
type inttree = Empty | Node of int * inttree * inttree

(* use this function in fromList *)
let rec insert t i =
  match t with
      Empty -> Node(i,Empty,Empty)
    | Node(j,l,r) -> 
      if i=j 
      then t 
      else if i < j 
      then Node(j,insert l i,r)
      else Node(j,l,insert r i)

(* no need for this function; it is just an example *)
let rec member t i =
  match t with
      Empty -> false
    | Node(j,l,r) -> i=j || (i < j && member l i) || member r i

(* put fromList, sum1, prod1, avg1, map and negateAll here *)
let rec listFromTree_rec t =
	match t with
		Empty -> []
	|	Node(j,l,r) -> (listFromTree_rec l) @ ( j :: (listFromTree_rec r))

let rec fromList_rec t x =
      match x with
      | [] -> t
      | hd :: tl -> fromList_rec (insert t hd) tl
	  
let fromList x =
	let unsorted = fromList_rec Empty x in
	let sorted_list = listFromTree_rec unsorted in
	fromList_rec Empty sorted_list

let rec sum1 t=
	match t with
			| Empty -> 0
			| Node(i,l,r) -> i + (sum1 l) + (sum1 r)
			
let rec prod1 t=
	match t with
		| Empty -> 1
		| Node(i,l,r) -> i * (prod1 l) * (prod1 r)
		
let rec count1 t=
	match t with 
		| Empty -> 0
		| Node(i,l,r) -> 1 + count1(l) + count1(r)

exception DivisionByZero
		
let avg1 t =
	let c = count1 t in
	match c with
	0 -> raise DivisionByZero
	|_ ->sum1 t / c

let rec map f t =
	match t with
	Empty -> Empty
	| Node(i,l,r) -> Node( (f i),(map f l),(map f r));;
	
let rec negateAll t =
      match t with
      | Empty -> Empty
      | Node(i,l,r) -> Node(~-i, negateAll(l), negateAll(r))
	
let rec fold f a t =
  match t with
      Empty -> a
    | Node(j,l,r) -> fold f (fold f (f a j) l) r

(* put sum2, prod2, and avg2 here *)

let sum2 t =
	fold (fun a j -> a + j) 0 t
	
let prod2 t =
	fold (fun a j -> a * j) 1 t

let count2 t =
	fold (fun a j -> a + 1) 0 t

let avg2 t =
	let c = count2 t in
	match c with
	0 -> raise DivisionByZero
	| _ -> sum2 t / c
	
(**)
	
type 'a iterator = Nomore | More of 'a * (unit -> 'a iterator)

let rec iter t =
  let rec f t k =
    match t with
	Empty -> k ()
      | Node(j,l,r) -> More(j, fun () -> f l (fun () -> f r k))
  in f t (fun () -> Nomore)

(*
	The 'a iterator type knows that either there is another iteration, or the iteration is done symbolized by the Nomore constant.
	
	The tree iterator creator function implements the function that knows how to advance the iterator into the next postion, 
	given the current position and context of the tree.
	
	a user of the iterator type & function needs to call iter to capture the context of the tree and 
	maintain the iterator state with the function that captures the outermost context of the tree. this capture is done on the last line with f t (fun() ->Nomore).
	
	in practical terms it means we need to match on More(j,l) ->.
	
	To advance the iterator we simply  call   l()  (this invokes a void argument function.).
	
	To recusively advance the iterator, we call the function, passing the result of the call to advance the iterator. 
	
*)
  
let rec count3_itr itr =
      match itr with
      Nomore -> 0
      |More(j,l) -> 1 + count3_itr (l())
	  
let count3 t =
	count3_itr (iter t)
	
let rec sum3_itr itr =
	match itr with
	Nomore -> 0
	|More(j,l) -> j + sum3_itr ( l())

let sum3 t =
	sum3_itr (iter t)

let rec prod3_itr itr =
	match itr with
	Nomore -> 1
	|More(j,l) -> j * prod3_itr ( l())
	
let prod3 t =
	prod3_itr (iter t)
	
let avg3 t =
	let c = count3 t in
	match c with
	0 -> raise DivisionByZero
	|_ ->sum3 t / c 

(* challenge problem: put optionToException and exceptionToOption here *)

let optionToException fnc exn =
      let ret = fun (a) ->
              let b = (fnc a) in
                      match b with
                      None -> raise exn
                      |Some y -> y
      in 
	  ret

let exceptionToOption atoB extBool =
      let ret = fun (a) ->
		try (atoB a) with exn -> 
			if (extBool exn) then None else raise exn
      in ret
	  
(*
		This does not compute as 
		('a -> 'b) -> (exn -> bool) -> 'a -> 'b option =
		
		is this because we return None, thus binding the type to 'b option, and when the inference rules
		are applied, the function atoB which could return a 'b, is bound more tightly to the 'b option?
		
		so we get the following definition:		
		('a -> 'b option) -> (exn -> bool) -> 'a -> 'b option =
		
		
		
		The reason the why optionToException takes an exn, 
		is the function will raise the appropriate 
		exception if the option comes back none.
		
		The reason why the exceptionToOption requires a exn to bool, if the exception is a 'normal'
		case flow, or it has been handled, then we can just return None, otherwise the implied
		answer is the continued raising of the exception.
		
		I take it that exn is a fundamental type of the ocaml language?  I found a slight reference here:
		
		http://caml.inria.fr/pub/docs/oreilly-book/html/book-ora017.html
		
		"In Objective CAML, exceptions belong to a predefined type exn.
		This type is very special since it is an extensible sum type: 
		the set of values of the type can be extended by declaring new constructors9.
		This detail lets users define their own exceptions by adding new constructors to the type exn."
		
	
*)


(* a little testing -- commented out since the functions do not exist yet *)


let tr = fromList [0;1;2;3;4;5;6;7;8;9;9;9;1] (* repeats get removed *)
let print_ans f t = print_string (string_of_int (f t)); print_string "\n"
let _ = print_ans sum1 tr
let _ = print_ans prod1 tr
let _ = print_ans avg1 tr
let _ = print_ans sum2 tr
let _ = print_ans prod2 tr
let _ = print_ans avg2 tr
let _ = print_ans sum3 tr
let _ = print_ans prod3 tr
let _ = print_ans avg3 tr

