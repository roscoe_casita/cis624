(* This file is the only one you should modify for problems 2 and 3 *)
open Ast
  
exception InterpUnimplemented
exception DoNotTakeStepWithSkip

(* There are simpler heap definitions, but these will be easier
   for extending/modifying to support pushheap, popheap, pushvar, and popvar *)
let empty_heap = []
let empty_list = []

let rec get_var heap str = 
  match heap with
      [] -> 0
    | (str',i,l)::tl -> if str=str' then i else get_var tl str
	
let rec set_var heap str num = 
  match heap with
      [] -> [(str,num,[])]
    | (str',i,l)::tl -> if str=str' 
                      then (str,num,l)::tl 
                      else (str',i,l)::(set_var tl str num)
					  
let rec push_heap h s l =
	let l1 = h::l in
	(h,s,l1)
	
let rec pop_heap h s l str =
	match l with
		[] -> (h,s,l)
	|	hd::tl ->((set_var hd str (get_var h str)),s,tl)
	
let rec push_var h str =
	match h with
	[] -> [(str,0,[0])]
	| (str',i,stack)::tl -> if str = str'
						then 
							(str',i,i::stack)::tl
						else 
							(str',i,stack)::push_var tl str
	
let rec pop_var h str =
	match h with
	[] -> [(str,0,[0])]
	| (str',i,stack)::tl -> if str = str'
						then 
							match stack with
							[] -> (str,i,[0])::tl
							| shd::stl -> (str',shd,stl)::tl									
						else 
							(str',i,stack)::pop_var tl str
	
let rec interp_exp h e =
  let interp_exp_r = interp_exp h in
  match e with
      Int i -> i
    | Var v -> get_var h v
    | Plus(e1,e2)  -> (interp_exp_r e1) + (interp_exp_r e2)
    | Times(e1,e2) -> (interp_exp_r e1) * (interp_exp_r e2)
 
let rec interp_step h s l=
  match s with
      Skip ->  raise DoNotTakeStepWithSkip 
    | Assign(v,e) -> (set_var h v (interp_exp h e), Skip, l)
    | Seq(Skip,s2) -> (h,s2,l)
    | Seq(s1,s2) -> 
      let (h3,s3,l3) = interp_step h s1 l in
      (h3, Seq(s3,s2),l3)
    | If(e,s1,s2) ->
      if((interp_exp h e) <= 0)
      then (h,s2,l)
      else (h,s1,l)
    | While(e,s1) -> (h, If(e,Seq(s1,s),Skip),l)
	| Pushheap -> (push_heap h Skip l)
	| Popheap(str) -> (pop_heap h Skip l str)
	| Pushvar(str) -> ((push_var h str),Skip,l)
	| Popvar(str) ->  ((pop_var h str),Skip,l)
    (*| _ -> raise InterpUnimplemented*)

let rec iter h l s =
  match (h,s) with
      (_,Skip) -> get_var h "ans"
    | _ -> let (h',s',l') = interp_step h s l in iter h' l' s'
  
let interp = iter empty_heap empty_list

(* the next two bindings really belong in a "main" module, but this
keeps all the code you need in one place *)

(* expect one command-line argument, a file to parse and interpret *)
(* you do not need to understand this interaction with the system *)
let get_prog () =
  let argv = Sys.argv in
  let _ = 
    if Array.length argv != 2
    then (prerr_string ("usage: " ^ argv.(0) ^ " [file-to-interpret]\n");
	  exit 1) in
  let ch = open_in argv.(1) in
  Parse.program Lex.lexer (Lexing.from_channel ch)

let _ =
  let prog = get_prog () in
  print_string ("ans = " ^ string_of_int (interp prog ) ^"\n")
