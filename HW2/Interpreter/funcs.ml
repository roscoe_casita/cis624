

let rec find heap x =
      match heap with
      (a,b)::c -> if a = x then b else find c x
      | [] -> 0
	  


let rec place heap str x =
      match heap with
      [] -> (str,x)::[]
      | (a,b)::c -> if a = str then (a,x)::c else (a,b)::((place c str x))