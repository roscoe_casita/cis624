(*

	Top definitions: helper print functions, both non & continuation style 'completions'
	

*)


let rec print_list_of_int_rec x = 
	match x with	
	[] -> (print_string "]"; print_newline()) 
	| head::[] -> print_int head; print_list_of_int_rec []
	| head::tail -> print_int head; print_string ";" ; print_list_of_int_rec tail ;;
		
let print_list_of_int x =
		print_string "["; print_list_of_int_rec x;;
	
let print_bool x = print_string (string_of_bool x); print_newline();;

let report x = print_string "Result: "; print_int x; print_newline();;

let print_int_k x = print_int x; print_newline();;
let print_float_k x = print_float x; print_newline();;
let print_string_k x = print_string x; print_newline();;
let print_list_of_int_k x = print_list_of_int x;;
let print_bool_k x = print_bool x;;
let neg_num_print i = 
	print_string ("Error: " ^ (string_of_int i) ^ " is not a whole number");
	print_newline();;

let endk = (fun x -> x);;

(*
	Begin problems :
*)

(* 
	Non- continuation style of basic math operators.
*)
let add 		a b = a + b;;
let sub 		a b = a - b;;
let times 		a b = a * b;;
let plus 		a b = a +. b;;
let take_away 	a b = a -. b;;
let mult 		a b = a *. b;;

(*
	non continuation style for basic 'type' operators (list, string, boolean equality.)
*)
let cat 	a b = a ^ b;;
let cons 	a b = match b with [] -> [a] | head::tail -> a::head::tail;;
let less 	a b = a < b;;
let eq 		a b = a = b;;
let lesseq  a b = a <= b;;

(*
	this defines a function which captures functions that take 2 parameters, and attaches 
	a continuation to attach, and attaches it.
*)
let attachk f a b k = k (f a b);;

(*
	define the continuation functions as calls to rec functions attaching a continuation to them.
	
	we test all these functions, then define them again as direct continuation passing functions.
*)

let addk 		 a b k = attachk add  		a b k ;;
let subk         a b k = attachk sub  		a b k ;;
let timesk       a b k = attachk times  	a b k ;;
let plusk        a b k = attachk plus 		a b k ;;
let take_awayk   a b k = attachk take_away  a b k ;;
let multk        a b k = attachk mult  		a b k ;;

let catk 		a b k = attachk cat  		a b k ;;
let consk       a b k = attachk cons  		a b k ;;
let lessk       a b k = attachk less  		a b k ;;
let eqk         a b k = attachk eq	 		a b k ;;
let lesseqk		a b k = attachk lesseq		a b k ;; 

addk 			1 	1	print_int_k;;
subk        	1 	1	print_int_k;;
timesk      	1 	1	print_int_k;;
plusk       	1. 	1.	print_float_k;;
take_awayk  	1. 	1.	print_float_k;;
multk       	1. 	1.	print_float_k;;

catk 			"hello" "world" print_string_k;;

consk       	1		[] 		print_list_of_int_k;;
lessk       	4		5 		print_bool_k;;
eqk         	false	true 	print_bool_k;;

lesseqk 		2 		2 		print_bool_k;;

print_newline();;
print_newline();;
(*
	define the continuation functions in pure call contuation and test again.
*)
let addk 		 a b k =  k (a + b) ;;
let subk         a b k =  k (a - b) ;;
let timesk       a b k =  k (a * b) ;;
let plusk        a b k =  k (a +. b);;
let take_awayk   a b k =  k (a -. b) ;;
let multk        a b k =  k (a *. b) ;;

let catk 		a b k = k (a ^ b);;
let consk       a b k = if b = [] then k ([a]) else k (a::b) ;;
let lessk       a b k = k (a < b);;
let eqk         a b k = k (a = b);;
let lesseqk		a b k = k (a <= b);;

addk 			1 	1	print_int_k;;
subk        	1 	1	print_int_k;;
timesk      	1 	1	print_int_k;;
plusk       	1. 	1.	print_float_k;;
take_awayk  	1. 	1.	print_float_k;;
multk       	1. 	1.	print_float_k;;

catk 			"hello" "world" print_string_k;;

consk       	1		[] 		print_list_of_int_k;;
lessk       	4		5 		print_bool_k;;
eqk         	false	true 	print_bool_k;;

lesseqk 		2 		2 		print_bool_k;;

print_newline();;
print_newline();;


(*
	Begin problem 2:
*)

let abcdk a b c d k = addk (timesk d a (fun x -> x)) (addk (timesk b c (fun x -> x)) a (fun x ->x))  k ;;

abcdk 2 3 4 5 report ;;

print_newline();;


(*
	Begin Problem 3.a.i: 
	
	first we define the factorial range function in recursive form.
	test the function.
*)

let rec fact_range n m = 
	if n <= 0 
		then 
			1
		else
			if n <= m then m
				else n * (fact_range (n-1) m);;
			

print_int (fact_range 5 1);;
print_newline();;
print_int (fact_range 7 5);;
print_newline();;
print_newline();;

(*
	Part one 3.a.ii, quick easy way out:  attack continuation k to normal rec function.
	
	we also define the function in full continuation style passing after we test.
*)
let fact_rangek a b k = attachk fact_range a b k;;
print_int (fact_range 5 1);;
print_newline();;
print_int (fact_range 7 5);;
print_newline();;
print_newline();;

(*
	Part two 3.a.ii
	now for the 'continuation' form with explicit continuation calls. 
*)

let rec fact_rangek x y z = 
	let rec fact_rangeki n m k =  
	lessk n m 
		(fun r -> 
			if r then k 1
			else eqk n 0 
				(fun b -> if b then k 1
					else 
						k (endk (timesk n (subk n 1 (fun x -> fact_rangeki x m k )) endk)))) in 
	z (fact_rangeki x y endk);;					

fact_rangek 5 1 report;;
print_newline();;
fact_rangek 7 5 report;;
print_newline();;
print_newline();;

(* Begin problem 4.a*)
(*recurive definition of apply all*)
let rec app_all flst x = 
	match flst with
		[] -> []
		|head::tail -> (head x)::(app_all tail x);;

print_list_of_int (app_all [((+) 1); (fun x -> x * x); (fun x -> 13)] 5);;
	
(* Begin problem 4.b*)
(*continuation definition of apply all*)
let app_allk flst x k = 
	let rec app_allkc flst1 x1 k1 = 
	match flst1 with
		[] -> k1 []
		|head::tail -> k1 ((head x endk)::(app_allkc tail x1 endk))
	in 
	k (app_allkc flst x endk);;
		
app_allk [(addk 1); (fun x -> timesk x x); (fun x -> (fun k -> k 13))] 5 print_list_of_int_k ;;
print_newline();;
(*begin problem 5.a*)

(*this continuation function returns either the sum or the negative number problem.*)
let rec sum_wholeskc l1 s1 k = 
		match l1 with 
		[] -> k s1
		| head::tail -> 
			lessk head 0 
				(fun y -> if y then k head 
					else
					let x = sum_wholeskc tail 0 endk in
					lessk x 0 
						(fun z -> if z then x 
							else 
							 k ( addk s1 head (fun a -> addk a x endk))));;
							 
(*this continuation function calls either xk if the result is negative or k if sum >0*)
let rec sum_wholesk l k xk = 
	let x = sum_wholeskc l 0 endk in
	lessk x 0 
		(fun b -> if b then xk x else k x);;
			
print_list_of_int [0; 1; 2; 3];;
sum_wholesk [0; 1; 2; 3] report neg_num_print;;
print_newline();;
print_list_of_int [0; -1; 2; 3];;
sum_wholesk [0; -1; 2; 3] report neg_num_print;;
print_newline();;



