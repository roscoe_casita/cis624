﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RegularLanguages
{

    public abstract class Expression
    {


        public Expression()
        {
            while ((GraphVizNodeID == null) || (GraphVizNodeID.CompareTo("") == 0))
            {
                Guid temp = Guid.NewGuid();
                string s = temp.ToString();

                s = s.Replace("-", "");

                s = s.Replace("0", "");
                s = s.Replace("1", "");
                s = s.Replace("2", "");
                s = s.Replace("3", "");
                s = s.Replace("4", "");
                s = s.Replace("5", "");
                s = s.Replace("6", "");
                s = s.Replace("7", "");
                s = s.Replace("8", "");
                s = s.Replace("9", "");

                GraphVizNodeID = s;
                ID = temp;
            }
        }

        public abstract string PrintStatement();

        

        public abstract bool Equal(Expression ex);
        public abstract Expression Copy();

        public string GraphVizNodeText { get; set; }
        public string GraphVizNodeID { get; set; }
        public Guid ID;
        public Expression Parent { get; set; }
    }

    public class Macro : Expression
    {
        public Macro(string name)
        {
            Name = name;
            GraphVizNodeText = name ;
            KeyValuePair<Macro,Expression> mid = HyperstaticExpressions[name];
            Replacement = mid.Value;
            ID = mid.Key.ID;

        }
        public Macro(string name, Expression replacement)
        {
            GraphVizNodeText = name;
            Name = name;
            Replacement = replacement;
            HyperstaticExpressions.Add(name,new KeyValuePair<Macro,Expression>(this,replacement) );
        }

        public readonly string Name;
        public readonly Expression Replacement;

        public override Expression Copy()
        {
            Macro m = new Macro(Name);
            m.ID = new Guid(ID.ToByteArray());
            return m;
        }
        public override string PrintStatement()
        {
            return Name;
        }

        public override bool Equal(Expression ex)
        {
            return (ID.CompareTo(ex.ID) == 0);
        }
        private static Dictionary<string, KeyValuePair<Macro, Expression>> HyperstaticExpressions = new Dictionary<string, KeyValuePair<Macro, Expression>>();

        public static Macro LoopupMacro(Expression ex)
        {
            if (ex is Macro)
                return (Macro)ex;
            Macro m = null;
            foreach (KeyValuePair<Macro, Expression> kvp in HyperstaticExpressions.Values)
            {
                if (kvp.Value.Equal(ex))
                {
                    m = (Macro)kvp.Key.Copy();
                    break;
                }
            }
            return m;
        }
    }

    public class Variable : Expression
    {
        public Variable(string name)
        {
            GraphVizNodeText = name;
            Name = name;
        }

        public readonly string Name;

        public override string PrintStatement()
        {
            return Name;
        }

        public override Expression Copy()
        {
            Variable v = new Variable(Name);
            v.ID = ID;
            return v;
        }

        public override bool Equal(Expression ex)
        {
            return (ID.CompareTo(ex.ID) == 0);
        }
    }

    public class Abstraction : Expression
    {
        public Abstraction(Variable v, Expression exp)
        {
            Parameter = v;
            Parameter.Parent = this;
            Body = exp;
            Body.Parent = this;
            GraphVizNodeText = "λ " + v.Name ;
        }

        public override string PrintStatement()
        {
            return "λ" + Parameter.Name + ".(" + Body.PrintStatement() + ")";
        }

        public Variable Parameter;
        public Expression Body;

        public override Expression Copy()
        {
            Variable v = (Variable)Parameter.Copy();
            Expression exp = (Expression)Body.Copy();

            Abstraction a = new Abstraction(v,exp);
            a.ID = ID;
            return a;
        }

        public override bool Equal(Expression ex)
        {
            if (ID.CompareTo(ex.ID) == 0)
            {
                Abstraction a = ex as Abstraction;
                if (Parameter.ID == a.Parameter.ID)
                {
                    return Body.Equal(a.Body);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    public class Apply : Expression
    {
        public Apply(Expression exe, Expression param)
        {
            Execute = exe;
            exe.Parent = this;
            Parameter = param;
            param.Parent = this;
            GraphVizNodeText = "Apply" ;
        }

        public Expression Execute;
        public Expression Parameter;

        public override string PrintStatement()
        {
            String left = Execute.PrintStatement();
            String right = Parameter.PrintStatement();


            return "(" + left + ") (" + right+")";
        }

        public override Expression Copy()
        {
            Expression exe = Execute.Copy();
            Expression param = Parameter.Copy();
            Apply a =  new Apply(exe, param);
            a.ID = ID;
            return a;
        }

        public override bool Equal(Expression ex)
        {
            if (ID.CompareTo(ex.ID) == 0)
            {
                Apply a = ex as Apply;
                if (Execute.Equal(a.Execute))
                {
                    return Parameter.Equal(a.Parameter);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    public class LambdaInterpreter
    {
        public LambdaInterpreter()
        {
        }


        public Expression ReAliasize(Expression ex)
        {

            Macro m = Macro.LoopupMacro(ex);   
            
            if(m !=null)
            {
                m.Parent = ex.Parent;
                return m;
            }
            if (ex is Variable)
            {
                return ex;
            }
            else if (ex is Abstraction)
            {
                Abstraction a = ex as Abstraction;

                a.Body = ReAliasize(a.Body);

                return a;
            }
            else if (ex is Apply)
            {
                Apply a = ex as Apply;

                a.Parameter = ReAliasize(a.Parameter);
                a.Execute = ReAliasize(a.Execute);
                return a;
            }
            else
            {
                return ex;
            }
        }

        private static Random r = new Random();
        public static string GenerateGraphviz(Expression tree,bool ExpandMacros)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("digraph \"Abstract Execution Tree\"{");

            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"Abstract Execution Tree\",");
            sb.AppendLine("size = \"12,12\"];");
            Stack<Expression> items = new Stack<Expression>();

            if (tree != null)
            {
                items.Push(tree);

            }
            while (items.Count > 0)
            {
                Expression item = items.Pop();

                if (item.Parent != null)
                {
                    sb.AppendLine(item.GraphVizNodeID + " -> " + item.Parent.GraphVizNodeID + ";");
                }
                else
                {
                }
               
                sb.AppendLine( item.GraphVizNodeID + "[label=\"" + item.GraphVizNodeText + "\"];");

                if (ExpandMacros && item is Macro)
                {
                    Macro m = item as Macro;

                    sb.AppendLine(m.GraphVizNodeID + " -> " + m.Replacement.GraphVizNodeID + ";" );
                    items.Push(m.Replacement);
                }
                else if(item is Apply)
                {
                    Apply a = item as Apply;

                    sb.AppendLine(item.GraphVizNodeID + " -> " + a.Execute.GraphVizNodeID + ";");
                    sb.AppendLine(item.GraphVizNodeID + " -> " + a.Parameter.GraphVizNodeID + ";");

                    items.Push(a.Parameter);
                    items.Push(a.Execute);

                }
                else if (item is Abstraction)
                {
                    Abstraction a = item as Abstraction;

                    string s = a.Parameter.GraphVizNodeID + r.Next().ToString();

                    sb.AppendLine(s + "[label=\"" + a.Parameter.Name + "\"];");


                    //if (FindSubNode(a.Parameter, a.Body))
                    //{
                    //    sb.AppendLine(s + " -> " + a.Parameter.GraphVizNodeID + ";");
                    //}

                    sb.AppendLine(item.GraphVizNodeID + " -> " + s + ";");
                    
                    sb.AppendLine(item.GraphVizNodeID + " -> " + a.Body.GraphVizNodeID+";");
                    items.Push(a.Body);
                }
                
            }

            sb.Append("}\r\n");

            return sb.ToString();
        }

        private static bool FindSubNode(Variable variable, Expression expression)
        {
            if (expression is Macro)
            {
                return false;
            }
            else if (expression is Variable)
            {
                return ((Variable)expression).Equal(variable);
            }
            else if (expression is Apply)
            {
                Apply a = (Apply)expression;

                if (FindSubNode(variable, a.Parameter) || FindSubNode(variable, a.Execute))
                {
                    return true;
                }
            }
            else if (expression is Abstraction)
            {
                return FindSubNode(variable, ((Abstraction)expression).Body);
            }
            return false; // throw new Exception("Invalid Expression type.");

        }

        private Expression Replace(Expression tree, Variable v, Expression value)
        {
            if (tree.Equal(v))
            {
                return value.Copy();
            }
            if(tree is Macro)
            {
                return tree;
            }
            else if (tree is Variable)
            {
                return tree;
            }
            else if (tree is Abstraction)
            {
                Abstraction l = tree as Abstraction;

                l.Body = Replace(l.Body, v, value);
                l.Body.Parent = l;

                return tree;
            }
            else if (tree is Apply)
            {
                Apply ex = tree as Apply;
                ex.Execute = Replace(ex.Execute, v, value);
                ex.Execute.Parent = ex;
                ex.Parameter = Replace(ex.Parameter, v, value);
                ex.Parameter.Parent = ex;
                return tree;
            }
            throw new Exception("Unknown Expression Type. Bad Replacement operation.");
        }

        public Expression NextInterpretedExpression(Expression tree, out bool finished)
        {
            Expression result = NextInterpretedExpressionI(tree, out finished);

            if (finished)
            {
                result = ReAliasize(result);
            }
            return result;
        }
        private Expression NextInterpretedExpressionI(Expression tree, out bool finished)
        {
            if (tree is Macro)
            {
                finished = false;
                Macro m = tree as Macro;
                Expression exp = m.Replacement.Copy();
                exp.Parent = null;
                return exp ;
            }
            else if (tree is Variable)
            {
                finished = true;
                tree.Parent = null;
                return tree;
            }
            else if (tree is Abstraction)
            {

                finished = true;
                tree.Parent = null;
                return tree;
            }
            else if (tree is Apply)
            {
                Apply ex = tree as Apply;

                if (ex.Execute is Variable)
                {
                    finished = true;
                    tree.Parent = null;
                    return tree;
                }
                else if (ex.Execute is Macro)
                {
                    finished = false;
                    Expression m = NextInterpretedExpressionI(ex.Execute, out finished);
                    m.Parent = ex;
                    ex.Execute = m;
                    return tree;
                }
                else if (ex.Execute is Apply)
                {
                    // we can't apply to a left hand apply, walk left and down and evaluate that sub expression first.

                    // theoretically, we can just adjust the pointer in the pointer structure here instead of recursive.

                    // this means we can eliminate the entire call stack with a single pointer. 
                    // Multiplied, by the addictional UP Pointer in every Cell.

                    Expression e = NextInterpretedExpressionI(ex.Execute, out finished);

                    ex.Execute = e;
                    e.Parent = ex;
                    return ex;
                }
                else if (ex.Execute is Abstraction)
                {
                    finished = false;
                   
                    Abstraction abs = ex.Execute as Abstraction;

                    Expression e = ex.Parameter;

                    if (e is Apply)
                    {

                            Expression ret = Replace(abs.Body, abs.Parameter, ex.Parameter);
                            ret.Parent = null;
                            return ret;
                        
                        return ex;
                    }
                    else
                    {
                        /// here we need to apply on the right side, if its apply, otherwise we call replace on an abstract, passing something 
                        /// other then an application. ... or just apply it... and replace it with whatever it is. 
                        /// 
                        Expression ret = Replace(abs.Body, abs.Parameter, ex.Parameter);
                        ret.Parent = null;
                        return ret;
                        /// here we need to link back up into the tree, and preserve parents.
                        /// or do we?  look up in apply, there we need to preserve the call. 
                        /// 
                    }
                }
                else
                {
                    throw new Exception("Unknown Expression Type To Interpret.");
                }
            }
            throw new Exception("Unknown Expression Type To Interpret.");
        }

        public Expression ConvertProgram(ParseTree tree)
        {
            if ((tree == null) || (tree.Rule == null) || (tree.Rule.Name == null) || (tree.Rule.Name != "<PROGRAM>"))
            {
                throw new Exception("Wrong First rule.");
            }
            if (tree.Decendants[0].Rule.Name == "<MACRO>")
            {
                string name = tree.Decendants[0].Decendants[0].Token;

                Expression e = ConvertTree(tree.Decendants[1]);
                Macro a = new Macro(name,e);
                return null;
            }
            else
            {
                return ConvertTree(tree.Decendants[0]);
            }
        }

        private Expression ConvertTree(ParseTree tree)
        {
            if (tree.Rule == null)
            {
                if (tree.Token == tree.Token.ToUpper())
                {
                    return new Macro(tree.Token);
                }
                else
                {
                    if (NamedCaptures.ContainsKey(tree.Token))
                    {
                        return NamedCaptures[tree.Token][0];
                    }
                    return new Variable(tree.Token);
                }
            }
            else
            {
                switch (tree.Rule.Name)
                {
                    case "<EXPRESSION_LIST>":
                        {
                           

                            ParseTree ptr = tree;

                            Expression return_value = null;
                           

                            do
                            {
                                if(ptr.Decendants.Count == 0)
                                {
                                    ptr = null;
                                }
                                else if (return_value == null)
                                {
                                    return_value = ConvertTree(ptr.Decendants[0]);
                                    
                                    ptr = ptr.Decendants[1];
                                }
                                else
                                {
                                    Expression new_return = ConvertTree(ptr.Decendants[0]);
                                    ptr = ptr.Decendants[1];
                                    return_value = new Apply(return_value,new_return);
                                }

                            }while(ptr != null);

                            if(return_value == null)
                            {
                                throw new Exception("This also should never happen.");
                                 
                            }
                             return return_value;

                        }

                    case "<EXPRESSION>":

                        return ConvertTree(tree.Decendants[0]);

                    case "<ABSTRACTION>":
                        {
                            Variable v = new Variable(tree.Decendants[0].Decendants[0].Token);

                            if (!NamedCaptures.ContainsKey(v.Name))
                            {
                                NamedCaptures.Add(v.Name, new List<Variable>());
                            }
                            NamedCaptures[v.Name].Insert(0, v);

                            Expression exp = ConvertTree(tree.Decendants[1]);
                            NamedCaptures[v.Name].RemoveAt(0);
                            if (NamedCaptures[v.Name].Count == 0)
                            {
                                NamedCaptures.Remove(v.Name);
                            }
                            Abstraction a = new Abstraction(v, exp);

                            return a;
                        }
                    case "<VARIABLE>":
                        {
                            if (NamedCaptures.ContainsKey(tree.Decendants[0].Token))
                            {
                                return NamedCaptures[tree.Decendants[0].Token][0].Copy();
                            }
                            else
                            {
                                return new Variable(tree.Decendants[0].Token);
                            }
                        }
                    case "<MACRO>":
                        {
                            return new Macro(tree.Decendants[0].Token);
                        }

                    default:
                        throw new Exception("Unknown Rule: " + tree.Rule.Name);
                }
            }
        }

        Dictionary<string, List<Variable>> NamedCaptures = new Dictionary<string, List<Variable>>();

        Dictionary<Macro, Expression> AliasTable = new Dictionary<Macro, Expression>();
    }

}
