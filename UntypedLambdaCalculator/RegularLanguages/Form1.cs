﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace RegularLanguages
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

          
        }

        private void ParseRules_Click(object sender, EventArgs e)
        {
            gp.Clear();
            foreach (string s in RULESBOX.Lines)
            {
                if (s.CompareTo("") == 0)
                    continue;
                if (s.StartsWith("#"))
                    continue;
                string test = gp.AddRule(s);

                if ((test!=null)&&(test.CompareTo("") != 0))
                {
                    Print("Added Rule: " + test);
                    continue;
                }

                Print("Failed to Parse Rule: " + s);
            }
            string graph = GrammarParser.GenerateGraphViz(gp);

            pictureBox1.Image = GetImage(graph);

        }


        StringBuilder sb = new StringBuilder();
        int max_count = 10000;
        int removal_count = 1000;
        void Print(string s)
        {
            s = s + "\r\n";
            sb.Insert(0, s);

            OutputConsole.Text = sb.ToString();

            if (sb.Length > max_count)
            {
                sb.Remove(max_count, sb.Length - (max_count+removal_count));
            }
        }
        GrammarParser gp = new GrammarParser();

       
        private void ParseProgram_Click(object sender, EventArgs e)
        {
            sb.Remove(0, sb.Length - 1);
            Parses.Clear();

            foreach (string s in PROGRAM_BOX.Lines)
            {
                if (s.CompareTo("") == 0)
                    continue;
                if (s.StartsWith("#"))
                    continue;

                ParseTree ProgramTree = null;
                string temp = new string(s.ToCharArray());
                if (gp.ParseProgram(ref temp, ref ProgramTree))
                {
                    Parses.Add(ProgramTree);
                    Print("Parsed Program: " + s);
                }
                else
                {
                    PrintParseTree(ProgramTree);
                    Print("Stopped at : " + temp);
                    Print("Failed to parse program:" + s);
                }
                if (ProgramTree != null)
                {
                    string graph = ParseTree.GenerateGraphviz(ProgramTree);

                    Image i = GetImage(graph);

                    pictureBox2.Image = i;
                }
            }
        }
        
        // get the image as a byte array.
        public Image GetImage(string graph)
        {
            string d = Directory.GetCurrentDirectory();
            string ImageFile = d + "\\GraphViz\\temp.png";
            string DotFile = d + "\\GraphViz\\temp.dot";


            if (File.Exists(ImageFile))
            {
                File.Delete(ImageFile);
            }

            if (File.Exists(DotFile))
            {
                File.Delete(DotFile);
            }
            File.WriteAllText(DotFile, graph);


            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C dot -Tpng temp.dot -o temp.png";
            startInfo.WorkingDirectory = d + "\\GraphViz\\";
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            if (!File.Exists(ImageFile))
                return null;

            Byte[] bytes = File.ReadAllBytes(ImageFile);
            Image i = Image.FromStream(new MemoryStream(bytes));
            return i;
        }



        void PrintParseTree(ParseTree at)
        {
            Stack<ParseTree> walk = new Stack<ParseTree>();

            if (at != null)
            {
                walk.Push(at);
            }
            while (walk.Count > 0)
            {
                at = walk.Pop();

                
                Print(at.ToString());

                for (int i = at.Decendants.Count - 1; i >= 0; i--)
                {
                    walk.Push(at.Decendants[i]);
                }
            }
        }

        List<ParseTree> Parses = new List<ParseTree>();
        List<Expression> Programs = new List<Expression>();
       
        private void InterpretProgram_Click(object sender, EventArgs e)
        {
            if (pulser == null)
            {
                InterpretStep();
            }
            else
            {
                Print("Please Wait for steps to finish.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (ParseTree pt in Parses)
            {
                Image i = GetImage(ParseTree.GenerateGraphviz(pt));
                pictureBox1.Image = i;

                Expression exp = li.ConvertProgram(pt);

                if (exp != null)
                {
                    Image j = GetImage(LambdaInterpreter.GenerateGraphviz(exp, false));
                    pictureBox2.Image = j;

                    Programs.Add(exp);
                }
            }
            Parses.Clear();
            PROGRAM_BOX.Clear();
        }
        LambdaInterpreter li = new LambdaInterpreter();

        void InterpretStep()
        {
            for(int i = 0; i < Programs.Count; i++)
            {
                Expression ex = Programs[i];
                Print(ex.PrintStatement());
                pictureBox1.Image = GetImage(LambdaInterpreter.GenerateGraphviz(ex, false));
                
                bool done = false;
                Expression ex1 = li.NextInterpretedExpression(ex, out done);
                pictureBox2.Image = GetImage(LambdaInterpreter.GenerateGraphviz(ex1, false));

                if (done)
                {
                    Programs.RemoveAt(0);
                    if (ex1 != null)
                    {
                        Print(ex1.PrintStatement());
                    }
                    Print("Finished Interpretation. Removing Program from list.");
                }
                else
                {
                    if (ex1 != null)
                    {
                        Programs[0] = ex1;
                    }
                    else
                    {
                        Programs.RemoveAt(0);
                        Print("Tree Evaluation returned null.");
                    }
                }
            }
        }
        Timer pulser = null;
        private void button2_Click(object sender, EventArgs e)
        {
            if (pulser == null)
            {
                pulser = new Timer();
                pulser.Tick += new EventHandler(pulser_Tick);
                pulser.Enabled = true;
                pulser.Start();
                pulser.Interval = 1;
            }
            else
            {
                Print("Please wait for Current Steps to finish.");
            }
        }

        void pulser_Tick(object sender, EventArgs e)
        {
            if (Programs.Count == 0)
            {
                pulser.Stop();
                pulser.Enabled = false;
                pulser.Dispose();
                pulser = null;
            }
            else
            {
                pulser.Interval = (int)numericUpDown1.Value;
                InterpretStep();
            }
        }
    }

}