﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RegularLanguages
{
    public partial class ShowImage : Form
    {
        public ShowImage()
        {
            InitializeComponent();
        }
        public ShowImage(Image i)
        {
            InitializeComponent();

            this.Width = i.Width;
            this.Height = i.Height;

            pictureBox1.Image = i;
        }

        private void ShowImage_Load(object sender, EventArgs e)
        {
            t.Interval = time;
            t.Tick += new EventHandler(t_Tick);
            t.Enabled = true;
            t.Start();

        }

        void t_Tick(object sender, EventArgs e)
        {
            t.Enabled = false;
            t.Stop();
            this.Close();
        }

        Timer t = new Timer();
        static int time = 2000;
    }
}
