﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RegularLanguages
{
    /// <summary>
    /// This represents the abstract parse tree, the general rule is that either a node is a control node (Grammar rule match), or a token rule match.
    /// 
    /// Thus Token could be null or Grammar Rule could be null, but not both.
    /// 
    /// if Token is not null, then Grammar Rule will be null, and Decendants will be empty.
    /// 
    /// This is not a binary tree, but rather a list of lists at every node. 
    /// </summary>
    public class ParseTree
    {
        /// <summary>
        /// Create a new abstract syntax parse tree node. 
        /// </summary>
        /// <param name="token">Token String match or Null</param>
        /// <param name="rule"></param>
        public ParseTree(ParseTree parent, string token, GrammarRule rule)
        {
            Parent = parent;
            Token = token;
            Rule = rule;

            while ((EasyName == null) || (EasyName.CompareTo("") == 0))
            {
                Guid ID = Guid.NewGuid();
                string s = ID.ToString();

                s = s.Replace("-", "");

                s = s.Replace("0", "");
                s = s.Replace("1", "");
                s = s.Replace("2", "");
                s = s.Replace("3", "");
                s = s.Replace("4", "");
                s = s.Replace("5", "");
                s = s.Replace("6", "");
                s = s.Replace("7", "");
                s = s.Replace("8", "");
                s = s.Replace("9", "");

                EasyName = s;
            }
        }

        public string GraphVizNodeName
        {
            get
            {
                if (Token == null)
                {
                    return EasyName + "[label=\"" + Rule.Name + "\"];";
                }
                else
                {
                    return EasyName + "[label=\"" + Token + "\"];";
                }
            }
        }

        /// <summary>
        /// Expose the properties of the node.
        /// </summary>
        public readonly string EasyName;
        public readonly string Token;
        public readonly GrammarRule Rule;
        public readonly ParseTree Parent;
        public readonly List<ParseTree> Decendants = new List<ParseTree>();

        /// <summary>
        /// Print the rule out as a linearization.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Rule == null)
            {
                return Token;
            }
            else
            {
                return Rule.Name;
            }
        }

        public static string GenerateGraphviz(ParseTree tree)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("digraph \"Abstract Parse Tree\"{");

            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"Abstract Parse Tree\",");
            sb.AppendLine("size = \"12,12\"];");

            Stack<ParseTree> items = new Stack<ParseTree>();
            items.Push(tree);

            while (items.Count > 0)
            {
                ParseTree item = items.Pop();
                sb.AppendLine(item.GraphVizNodeName);

                foreach (ParseTree at in item.Decendants)
                {
                    sb.AppendLine(item.EasyName + " -> " + at.EasyName + ";");
                    items.Push(at);
                }
            }

            sb.Append("}\r\n");

            return sb.ToString();
        }
    }

    /// <summary>
    /// This represents a token matching terminal rule. 
    /// 
    /// The rule has a name, and list of regular expressions that satisfy the rule.
    /// 
    /// 
    /// </summary>
    public class RegularRule
    {
        /// <summary>
        ///  Initilize the terminal rule with a name, and a default regular expression. More Regex can be added later, but we need at least one.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="rule"></param>
        public RegularRule(string name, Regex rule)
        {
            _Name = name;
            Rules = new List<Regex>();
            Rules.Add(rule);

            while ((EasyName == null) || (EasyName.CompareTo("") == 0))
            {
                Guid ID = Guid.NewGuid();
                string s = ID.ToString();

                s = s.Replace("-", "");

                s = s.Replace("0", "");
                s = s.Replace("1", "");
                s = s.Replace("2", "");
                s = s.Replace("3", "");
                s = s.Replace("4", "");
                s = s.Replace("5", "");
                s = s.Replace("6", "");
                s = s.Replace("7", "");
                s = s.Replace("8", "");
                s = s.Replace("9", "");

                EasyName = s;
            }
        }
        public string GetDoGraphVizName
        {
            get
            {
                return EasyName + "[label=\"" + Name + "\"];";
            }
        }
        public string EasyName;

        /// <summary>
        /// Add an additional regular expression terminal rule.
        /// </summary>
        /// <param name="rule"></param>
        public void AddRule(Regex rule)
        {
            Rules.Add(rule);
        }

        public string Name
        {
            get
            {
                return _Name;
            }
        }

        private string _Name;
        private List<Regex> Rules;

        /// <summary>
        /// Attempt to match the stream input to the regular expressions we have.
        /// 
        /// If any match the input stream, we remove the entire match (not the capture) from the stream.
        /// 
        /// This will advance and produce a token in the system.
        /// 
        /// The token will be consumed by the grammar rule and continue on.
        /// </summary>
        /// <param name="input_stream"></param>
        /// <returns></returns>
        public string Parse(ref string input_stream)
        {
            foreach (Regex rx in Rules)
            {
                Match m = rx.Match(input_stream);

                if (m.Groups.Count > 0)
                {
                    if (m.Groups[0].Captures.Count > 0)
                    {
                        string s = m.Groups[0].Captures[0].Value.Trim();

                        input_stream = input_stream.Remove(0, m.Length);

                        return s;
                    }
                }
            }
            return null;
        }
    }

    /// <summary>
    /// This represent an abstract grammar rule. 
    /// 
    /// Each rule has a list of a list of rules that need to be satisfied. 
    /// 
    /// They are checked inorder, so rules with multiple conditions EXPRESSION = ID | LAMBDA ID DOT EXPRESSION
    /// 
    /// Then ID will be checked first, so that determinism and back tracking is maintained.
    /// 
    /// </summary>
    public class GrammarRule
    {
        public GrammarRule(string name, List<string> rules)
        {
            _Name = name;
            Rules = new List<List<string>>();
            Rules.Add(rules);


            while ((EasyName == null) || (EasyName.CompareTo("") == 0))
            {
                Guid ID = Guid.NewGuid();
                string s = ID.ToString();

                s = s.Replace("-", "");

                s = s.Replace("0", "");
                s = s.Replace("1", "");
                s = s.Replace("2", "");
                s = s.Replace("3", "");
                s = s.Replace("4", "");
                s = s.Replace("5", "");
                s = s.Replace("6", "");
                s = s.Replace("7", "");
                s = s.Replace("8", "");
                s = s.Replace("9", "");

                EasyName = s;
            }
        }
        public string GetDoGraphVizName
        {
            get
            {
                return EasyName + "[label=\"" + Name + "\"];";
            }
        }
        public string EasyName;
        public string Name
        {
            get
            {
                return _Name;
            }
        }

        public void AddRule(List<string> rules)
        {
            Rules.Add(rules);
        }

        public List<List<string>> EnumerateRules()
        {
            return Rules;
        }

        private List<List<string>> Rules;
        private String _Name;
    }

    /// <summary>
    /// This is the Grammar/Regex parse. This accepts Token Grammar rules, and Grammar parse rules. 
    /// 
    /// Before parsing a program, all of the rules should be added.
    /// </summary>
    public class GrammarParser
    {

        public GrammarParser()
        {

        }

        /// <summary>
        /// This is the main parse routine that boot straps the parser by assuming there is a rule called PROGRAM
        /// </summary>
        /// <param name="input_stream"></param>
        /// <param name="at"></param>
        /// <returns></returns>
        public bool ParseProgram(ref string input_stream, ref ParseTree at)
        {
            at = new ParseTree(null, null, GrammarRules["<PROGRAM>"]);

            at = ParseSegment(ref input_stream, at);

            if (input_stream.Length > 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// This is a partial function that is called on every RULE in the grammar.
        /// For each List of RULEs in the given grammar RULE, we do the following:
        ///     1. Preserve the input stream so that backtracking can occur.
        ///     2. Parse ALL of the given rules (and ensure they match).
        ///     3. Update the input stream if the parse worked, and return this node.
        /// </summary>
        /// <param name="input_stream"></param>
        /// <param name="tree"></param>
        /// <returns></returns>
        private ParseTree ParseSegment(ref string input_stream, ParseTree tree)
        {

            foreach (List<string> rule in tree.Rule.EnumerateRules())
            {
                string temp = new string(input_stream.ToCharArray());

                List<ParseTree> decendants = Parse(ref temp, rule, tree);

                if (decendants != null)
                {
                    input_stream = temp;

                    foreach (ParseTree at in decendants)
                    {
                        if ((at.Rule == null) || (!DoNotAddRules.Contains(at.Rule.Name)))
                        {
                            //tree.Decendants.Insert(0, at);
                            tree.Decendants.Add(at);
                        }
                    }
                    return tree;
                }
            }
            return null;
        }

        /// <summary>
        /// This will parse a given input stream, against the list of rules, ensuring ALL RULES MATCH. (no exceptions.)
        /// 
        /// If not all the rules match, then we return nothing.
        /// </summary>
        /// <param name="input_stream"></param>
        /// <param name="rules"></param>
        /// <returns></returns>
        private List<ParseTree> Parse(ref string input_stream, List<string> rules, ParseTree parent)
        {
            List<ParseTree> parse_tree = new List<ParseTree>();
            foreach (string rule in rules)
            {
                if (GrammarRules.ContainsKey(rule))
                {
                    // if we have matched a given grammar rule, we need to start the whole recursive decent again, preserving input again, so we call parse segment.
                    ParseTree temp = new ParseTree(parent, null, GrammarRules[rule]);
                    ParseTree at = ParseSegment(ref input_stream, temp);
                    if (at == null)
                    {
                        return null;
                    }
                    parse_tree.Add(at);
                }
                else
                {
                    /// if we have not matched a given grammar rule, we call the token parser and pass it the input stream and the rule.
                    /// 
                    string token = ParseRegularRule(ref input_stream, rule);

                    if (token == null)
                    {
                        return null;
                    }
                    /// this is a terminal in the tree, so there is no grammar rule, just the string token we've matched.
                    parse_tree.Add(new ParseTree(parent, token, null));
                }
            }
            return parse_tree;
        }

        public void Clear()
        {
            this.GrammarRules.Clear();
            this.RegularRules.Clear();
            this.DoNotAddRules.Clear();
        }
        /// <summary>
        /// Add a new Grammar or Regular expression rule.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string AddRule(string s)
        {
            Match m = NewGrammarRule.Match(s);

            string name = m.Groups["GRAMMARRULENAME"].Value.Trim();

            if (name.CompareTo("") != 0)
            {
                List<string> rules = new List<string>();
                foreach (Capture c in m.Groups["GRAMMARRULE"].Captures)
                {
                    rules.Add(c.Value.Trim());
                }

                if (name.CompareTo("<DO_NOT_ADD_RULE>") == 0)
                {
                    DoNotAddRules.AddRange(rules);
                }
                else
                {

                    if (!GrammarRules.ContainsKey(name))
                    {
                        GrammarRule gr = new GrammarRule(name, rules);
                        GrammarRules.Add(name, gr);
                    }
                    else
                    {
                        GrammarRules[name].AddRule(rules);
                    }
                }
            }
            else
            {
                name = AddTokenRule(s);
            }
            return name;
        }



        /// <summary>
        /// This regular expression defines how to add new grammar rules.
        /// </summary>
        Regex NewGrammarRule = new Regex(@"^(?<GRAMMARRULENAME><\w+>)::==(?<GRAMMARRULE>(<\w+>|\w+))+", RegexOptions.Compiled);

        /// <summary>
        ///  this is the container of rules that are contained in the grammar.
        /// </summary>
        Dictionary<string, GrammarRule> GrammarRules = new Dictionary<string, GrammarRule>();


        /// <summary>
        ///  This handles the token regular expression rules. It will add a given regular expression rule to the table.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string AddTokenRule(string s)
        {
            Match m = NewRegularRule.Match(s);

            string name = m.Groups["RULENAME"].Value;
            if (name.CompareTo("") != 0)
            {
                Regex match = new Regex(m.Groups["REGULAREXPRESSION"].Value);

                if (!RegularRules.ContainsKey(name))
                {
                    RegularRule rr = new RegularRule(name, match);
                    RegularRules.Add(name, rr);
                }
                else
                {
                    RegularRules[name].AddRule(match);
                }
            }
            else
            {
                name = null;
            }
            return name;
        }
        /// <summary>
        /// This will find the given regular expression rule, and parse it, passing the input stream.
        /// </summary>
        /// <param name="input_stream"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        public string ParseRegularRule(ref string input_stream, string rule)
        {
            return RegularRules[rule].Parse(ref input_stream);
        }
        Regex NewRegularRule = new Regex(@"^(?<RULENAME>[A-Z][A-Z_0-9]*):=(?<REGULAREXPRESSION>\^.*)", RegexOptions.Compiled);
        Dictionary<String, RegularRule> RegularRules = new Dictionary<string, RegularRule>();
        List<string> DoNotAddRules = new List<string>();


        public static string GenerateGraphViz(GrammarParser gp)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("digraph \"Grammar Parser\"{");

            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"Grammar Parser\",");
            sb.AppendLine("size = \"12,12\"];");


            foreach (KeyValuePair<string, GrammarRule> kvp in gp.GrammarRules)
            {
                sb.AppendLine(kvp.Value.GetDoGraphVizName);

            }

            foreach (KeyValuePair<string, RegularRule> kvp in gp.RegularRules)
            {
                sb.AppendLine(kvp.Value.GetDoGraphVizName);
            }


            foreach (KeyValuePair<string, GrammarRule> kvp in gp.GrammarRules)
            {
                foreach (List<string> lists in kvp.Value.EnumerateRules())
                {
                    foreach (string node in lists)
                    {
                        string from = kvp.Value.EasyName;
                        string to = null;
                        if (gp.GrammarRules.ContainsKey(node))
                        {
                            to = gp.GrammarRules[node].EasyName;

                        }
                        else
                        {
                            to = gp.RegularRules[node].EasyName;
                        }
                        sb.AppendLine("\t" + from + " -> " + to + ";");
                    }
                }
            }

            sb.Append("}\r\n");

            return sb.ToString();
        }
    }
}
