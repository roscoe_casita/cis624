type 'a mu = Roll of ('a mu -> 'a);;
let unroll (Roll x) = x;;
let fix f = (fun x a -> f (unroll x x) a) (Roll (fun x a -> f (unroll x x ) a ));;

let ack f =
      let ack_i x y=
              match x,y with
              0,n -> (n+1)
              |m,0 -> (f (m-1) 1)
              |m,n -> (f (m-1) (f m (n-1)))
      in
      ack_i;;
	  
let ackerman x y =
	fix ack x y;;
